-- MySQL Script generated by MySQL Workbench
-- Wed Jan  8 00:48:10 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`styles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`styles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `section` VARCHAR(45) NOT NULL,
  `sub_section` VARCHAR(45) NOT NULL,
  `sub_sub_section` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`questions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `question` TEXT NULL,
  `question_ref` INT NULL,
  `type` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`survey`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`survey` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  `respondent_id` INT NOT NULL,
  `question_id` INT NOT NULL,
  `response` TEXT NULL,
  `section` INT NULL,
  `sub_section` INT NULL,
  `sub_sub_section` INT NULL,
  `number` INT NULL,
  `numbered` BINARY NULL,
  `styles_id` INT NOT NULL,
  `questions_id` INT NOT NULL,
  `questions_survey_id` INT NOT NULL,
  PRIMARY KEY (`id`, `styles_id`, `questions_id`, `questions_survey_id`),
  INDEX `fk_survey_styles1_idx` (`styles_id` ASC) VISIBLE,
  INDEX `fk_survey_questions1_idx` (`questions_id` ASC, `questions_survey_id` ASC) VISIBLE,
  CONSTRAINT `fk_survey_styles1`
    FOREIGN KEY (`styles_id`)
    REFERENCES `mydb`.`styles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_survey_questions1`
    FOREIGN KEY (`questions_id`)
    REFERENCES `mydb`.`questions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`people`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`people` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `middle_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `suffix` VARCHAR(45) NULL,
  `email` VARCHAR(225) NULL,
  `address` VARCHAR(45) NULL,
  `phone_number` VARCHAR(45) NULL,
  `biography` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`surveys_people`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`surveys_people` (
  `survey_id` INT NOT NULL,
  `people_id` INT NOT NULL,
  `title` VARCHAR(45) NULL,
  `audit` BINARY NULL,
  `compensation` BINARY NULL,
  `nom_gov` BINARY NULL,
  `risk` BINARY NULL,
  `hr` BINARY NULL,
  `language` VARCHAR(45) NULL,
  PRIMARY KEY (`survey_id`, `people_id`),
  INDEX `fk_survey_has_people_people1_idx` (`people_id` ASC) VISIBLE,
  INDEX `fk_survey_has_people_survey_idx` (`survey_id` ASC) VISIBLE,
  CONSTRAINT `fk_survey_has_people_survey`
    FOREIGN KEY (`survey_id`)
    REFERENCES `mydb`.`survey` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_survey_has_people_people1`
    FOREIGN KEY (`people_id`)
    REFERENCES `mydb`.`people` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
